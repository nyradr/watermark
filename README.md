# Watermark pdf

Locally add watermark to pdf documents.
All the documents uploaded to the service stays locally.


Currently hosted [here](https://watermarkpdf.pages.dev/).

## Development setup

Install dependencies :
`npm install`

Start development server:
`npm start`

Build for production:
`npm run build`
