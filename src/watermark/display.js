import React from "react";


export class Display extends React.Component{
    render() {
        if (this.props.pdf.length === 0) {
            return (
                <div class="w-100 p-5">
                    <div class="alert alert-danger mt-5" role="alert">
                        Please upload a valid PDF file.
                    </div>
                </div>
            );
        } else {
            return (
                <iframe class="w-100" height="99%" src={ this.props.pdf } title="PDF preview"></iframe>
            );
        }
    }
}